# Proxy Monitoring

The Proxy Monitoring is a Docker Stack project, with a Traefik container along some monitoring tools.


## How to use this repository

You must create a .env file on the project root, adding at least the following variables:

`DOMAIN=`

`GF_SECURITY_ADMIN_PASSWORD=`

`GF_USERS_ALLOW_SIGN_UP=false`

`GF_INSTALL_PLUGINS=grafana-piechart-panel`

To deploy the stack, launch this command on the server:

`docker-compose config | docker stack deploy --compose-file - proxy`


## Contribution

This is an open source module and can be used as a base for other modules. If you have suggestions for improvements, 
just submit your pull request.

## Versioning

We use SemVer to versioning. To view all versions for this module, visit the tags page, on this repository.

## Authors

Andre Gugliotti - Initial development - [AndreGugliotti](https://gitlab.com/andregugliotti)

Lorenzo Calamandrei - Initial development - [Lorenzo Calamandrei](https://gitlab.com/lorenzocalamandrei)

See also the developers list, with all those who contributed to this project.
 [Contributors](https://gitlab.com/andregugliotti/docker-swarm-proxy-and-monitoring/-/project_members)

## License

This project is licensed under GNU General Public License, version 3.